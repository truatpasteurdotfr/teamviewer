%define _enable_debug_packages %{nil}
%define debug_package          %{nil}

Summary: Remote control and meeting solution
Name: teamviewer
Version: 14.6.2452
Release: 2%{?dist}
URL: https://www.teamviewer.com/en/products/teamviewer/
# version=14.6.2452; wget -L https://download.teamviewer.com/download/linux/teamviewer_amd64.tar.xz#/teamviewer-%{version}.tar.xz --trust-server-names
Source0: https://download.teamviewer.com/download/linux/teamviewer_amd64.tar.xz#/teamviewer_%{version}_amd64.tar.xz
License: Proprietary
BuildRequires: desktop-file-utils
BuildRequires: sed
BuildRequires: systemd
Requires(post): %{_bindir}/mktemp
Requires(post): %{_sbindir}/semodule
Requires(postun): %{_sbindir}/semodule
%if %{?el7}%{?el6}
Requires: dbus
%else
requires: dbus-common
%endif
Requires: hicolor-icon-theme
Requires: polkit
Requires: qt5-qtquickcontrols
ExclusiveArch: x86_64

%description
TeamViewer provides easy, fast and secure remote access and meeting solutions
to Linux, Windows PCs, Apple PCs and various other platforms,
including Android and iPhone.

TeamViewer is free for personal use.
You can use TeamViewer completely free of charge to access your private
computers or to help your friends with their computer problems.

To buy a license for commercial use, please visit http://www.teamviewer.com

This package contains Free Software components.
For details, see /usr/share/doc/teamviewer/license_foss.txt

%prep
%setup -q -n %{name}
# Change hard-coded paths in scripts
sed -i -e "s,/opt/teamviewer,%{_libdir}/%{name},g" \
  tv_bin/desktop/com.teamviewer.TeamViewer.desktop \
  tv_bin/script/com.teamviewer.TeamViewer{,.Desktop}.service \
  tv_bin/script/teamviewerd.RHEL.conf \
  tv_bin/script/teamviewerd.service \
  tv_bin/script/teamviewer_setup \
  tv_bin/script/tvw_*

# Switch operation mode from 'portable' to 'installed'
sed -i -e "s/TAR_NI/TAR_IN/g" tv_bin/script/tvw_config

%build

%install
install -dm755 %{buildroot}{/etc/%{name},%{_bindir},/var/log/%{name}14}
install -dm755 %{buildroot}%{_unitdir}
install -dm755 %{buildroot}%{_libdir}/%{name}/tv_bin/{resources,script}
install -dm755 %{buildroot}%{_datadir}/dbus-1/services
install -dm755 %{buildroot}%{_datadir}/polkit-1/actions
for sz in 16 20 24 32 48 256 ; do
  install -Dpm644 tv_bin/desktop/%{name}_${sz}.png \
    %{buildroot}%{_datadir}/icons/hicolor/${sz}x${sz}/apps/TeamViewer.png
done
install -pm755 -t %{buildroot}%{_libdir}/%{name}/tv_bin \
  tv_bin/{TeamViewer{,_Desktop},teamviewer{d,-config}}
install -pm755 -t %{buildroot}%{_libdir}/%{name}/tv_bin/resources \
  tv_bin/resources/TVResource_*.so
install -pm755 -t %{buildroot}%{_libdir}/%{name}/tv_bin/script \
  tv_bin/script/teamviewer
desktop-file-install --vendor "" --dir %{buildroot}%{_datadir}/applications \
  tv_bin/desktop/com.teamviewer.TeamViewer.desktop
install -pm644 -t %{buildroot}%{_datadir}/dbus-1/services \
  tv_bin/script/com.teamviewer.TeamViewer{,.Desktop}.service
install -pm644 -t %{buildroot}%{_datadir}/polkit-1/actions \
  tv_bin/script/com.teamviewer.TeamViewer.policy
install -pm644 -t %{buildroot}%{_libdir}/%{name}/tv_bin/script \
  tv_bin/script/tvw_*
install -pm644 -t %{buildroot}%{_unitdir} \
  tv_bin/script/teamviewerd.service

ln -s %{_libdir}/%{name}/tv_bin/script/%{name} %{buildroot}%{_bindir}/%{name}
ln -s /etc/%{name} %{buildroot}%{_libdir}/%{name}/config
ln -s /var/log/%{name}14 %{buildroot}%{_libdir}/%{name}/logfiles

# generate localized files list
find %{buildroot} -type f -o -type l|sed '
s:'"%{buildroot}"'::
s:\(.*/resources/TVResource_\)\([^/_]\+\)\(.*\.so$\):%lang(\2) \1\2\3:
s:^\([^%].*\)::
s:%lang(C) ::
s:zh:zh_:
/^$/d' > %{name}.lang

%preun
if [ $1 -eq 0 ]; then
  %{_libdir}/%{name}/tv_bin/teamviewerd --unmanage
fi
%systemd_preun teamviewerd.service

%post
TMPDIR=$(%{_bindir}/mktemp -d)
cat >> $TMPDIR/%{name}-rpm.cil << __EOF__
(allow init_t etc_t(file (append unlink)))
(allow init_t framebuf_device_t(chr_file (ioctl open read write)))
(allow init_t var_log_t(file (create)))
(allow init_t vnc_port_t(tcp_socket (name_connect)))
__EOF__
%{_sbindir}/semodule -i $TMPDIR/%{name}-rpm.cil
rm -rf $TMPDIR
%systemd_post teamvierwerd.service

%postun
%systemd_postun_with_restart teamvierwerd.service
if [ $1 -eq 0 ]; then
  %{_sbindir}/semodule -r %{name}-rpm > /dev/null || :
fi

%files -f %{name}.lang
%license doc/CopyRights_EN.txt doc/license_foss.txt doc/License_Full.txt doc/License.txt
%lang(de) %license doc/CopyRights_DE.txt doc/Lizenz_Full.txt doc/Lizenz.txt
%dir /etc/%{name}
%attr(0644,root,root) %config(noreplace) %ghost /etc/%{name}/global.conf
%{_bindir}/%{name}
%{_unitdir}/teamviewerd.service
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/config
%{_libdir}/%{name}/logfiles
%dir %{_libdir}/%{name}/tv_bin
%dir %{_libdir}/%{name}/tv_bin/script
%{_libdir}/%{name}/tv_bin/script/%{name}
%{_libdir}/%{name}/tv_bin/script/tvw_*
%{_libdir}/%{name}/tv_bin/TeamViewer
%{_libdir}/%{name}/tv_bin/TeamViewer_Desktop
%{_libdir}/%{name}/tv_bin/teamviewer-config
%{_libdir}/%{name}/tv_bin/teamviewerd
%{_datadir}/applications/com.teamviewer.TeamViewer.desktop
%{_datadir}/dbus-1/services/com.teamviewer.TeamViewer.Desktop.service
%{_datadir}/dbus-1/services/com.teamviewer.TeamViewer.service
%{_datadir}/icons/hicolor/*/apps/TeamViewer.png
%{_datadir}/polkit-1/actions/com.teamviewer.TeamViewer.policy
/var/log/%{name}14

%changelog
* Fri Oct  4 2019 Tru Huynh <tru@pasteur.fr> - 14.6.2452-2.%dist
- Require fix: dbus on EL6/7 but dbus-common on fedora
- adding %dist for re-use on el6/el7
- vendor tarball fix (2 underscores) in the filename 

* Fri Oct 04 2019 Dominik Mierzejewski <rpm@greysector.net> 14.6.2452-2
- fix build with coreutils < 8.23 (e.g. RHEL7)
- keep arch in tarball file name (Tru Huynh)

* Fri Oct 04 2019 Dominik Mierzejewski <rpm@greysector.net> 14.6.2452-1
- update to 14.6.2452
- make downloaded tarball versioned

* Sat Aug 24 2019 Dominik Mierzejewski <rpm@greysector.net> 14.5.1691-1
- update to 14.5.1691

* Mon Jul 22 2019 Dominik Mierzejewski <rpm@greysector.net> 14.4.2669-1
- update to 14.4.2669

* Fri Apr 12 2019 Dominik Mierzejewski <rpm@greysector.net> 14.2.2558-1
- update to 14.2.2558
- fix locations of icons

* Wed Jan 02 2019 Dominik Mierzejewski <rpm@greysector.net> 14.1.3399-1
- update to 14.1.3399

* Mon Nov 19 2018 Dominik Mierzejewski <rpm@greysector.net> 14.0.12762-1
- initial build
